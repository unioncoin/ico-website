# ico-website

The web application for monitoring the UnionCoin token distribution.

View the latest preview at [pre.ico.unioncoin.io](https://pre.ico.unioncoin.io/).
