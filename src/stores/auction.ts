import { observable } from 'mobx'
import Web3 from 'web3'
import ABI from './auctionABI'
import BN from 'bn.js';

export default class AuctionStore {
  @observable auctionStart: string|BN = '0'
  @observable cycleLengthSeconds: string|BN = '0'
  @observable tokensPerCycle: string|BN = '0'
  @observable totalCycleCount: string|BN = '0'
  @observable currentCycle: number = 0
  @observable currentPrice: string|BN = '0'
  @observable currentCycleStart: string|BN = '0'
  @observable currentCycleTokensSold: string|BN = '0'
  private web3: Web3
  contractAddress: string = '0x5a880f00778fc69012e78d831e2d5114bf46cbc6'
  private contract: any
  constructor() {
    this.web3 = new Web3(new Web3.providers.HttpProvider('https://rinkeby.infura.io/v3/b1a4ca90c9964c329c07874b37e05436'))
    this.contract = new this.web3.eth.Contract(ABI, this.contractAddress)
    Promise.all([
      this.contract.methods.startTimestamp().call(),
      this.contract.methods.cycleLengthSeconds().call(),
      this.contract.methods.tokensPerCycle().call(),
      this.contract.methods.totalCycleCount().call(),
    ])
      .then(([ _auctionStart, _cycleLengthSeconds, _tokensPerCycle, _totalCycleCount]) => {
        this.auctionStart = _auctionStart
        this.cycleLengthSeconds = _cycleLengthSeconds
        this.tokensPerCycle = _tokensPerCycle
        this.totalCycleCount = _totalCycleCount
      })
    setTimeout(() => {
      this.loadCurrentCycle()
      this.loadCurrentPrice()
    }, 15000)
  }

  async loadCurrentCycle() {
    this.currentCycle = await this.contract.methods.currentCycle().call()
  }

  get currentEtherPrice() {
    return this.web3.utils.fromWei(this.currentPrice.toString())
  }

  async loadCurrentPrice() {
    this.currentPrice = await this.contract.methods.currentTokenWeiPrice().call()
  }

  currentCycleTimeRemaining() {
    const seconds = Math.floor(Math.max(+this.cycleLengthSeconds.toString() - (+new Date()/1000 - (+this.auctionStart.toString() + +this.currentCycle.toString() * +this.cycleLengthSeconds.toString())), 0))
    const hours = Math.floor(seconds / 60 / 60)
    const minutes = Math.floor((seconds - hours * 60 * 60) / 60)
    const _seconds = seconds - hours * 60 * 60 - minutes * 60
    return `${hours}:${minutes}:${_seconds}`
  }
}
