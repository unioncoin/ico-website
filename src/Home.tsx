import React from 'react'
import { inject, observer } from 'mobx-react'
import AuctionStore from './stores/auction'

@inject('auction')
@observer
export default class Home extends React.Component<{
  auction: AuctionStore
}> {
  state = {
    cycleTimeRemaining: 0,
  }
  timer: any

  async componentDidMount() {
    this.timer = setInterval(() => {
      this.setState({
        cycleTimeRemaining: this.props.auction.currentCycleTimeRemaining(),
      })
    }, 1000)
    await Promise.all([
      this.props.auction.loadCurrentPrice(),
      this.props.auction.loadCurrentCycle(),
    ])
  }

  componentWillUnmount() {
    clearInterval(this.timer)
    this.timer = null
  }

  render() {
    return (
      <>
        <div>Current Auction Cycle</div>
        <div>{this.props.auction.currentCycle.toString()}/{this.props.auction.totalCycleCount.toString()}</div>
        <div>Current Token Price</div>
        <div>{this.props.auction.currentEtherPrice.toString()}</div>
        <div>Cycle Time Remaining</div>
        <div>{this.state.cycleTimeRemaining}</div>
      </>
    )
  }
}
